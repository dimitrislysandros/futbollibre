﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerBallControl : MonoBehaviour {

    public bool hasBall;
    Rigidbody rbBall;
    GameObject Ball;
    bool ballNear;
    public float h;
    public float v;

    // Use this for initialization
    void Start () {
        ballNear = false;
        h = 0;
        v =0;
	}
	
	// Update is called once per frame
	void Update () {
        this.GetComponent<ThirdPersonCharacter>().hasBall = hasBall;
        this.GetComponent<ThirdPersonUserControl>().hasBall = hasBall;
        //if  (hasBall&&!ballNear)
        //{
        //}
        h = GetComponent<ThirdPersonUserControl>().hReal;
        v = GetComponent<ThirdPersonUserControl>().vReal;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {

            hasBall = true;
            Vector3 push=v* Vector3.forward + h * Vector3.right;
            collision.gameObject.GetComponent<Rigidbody>().AddForce(push*2f, ForceMode.Impulse);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {

        }
        else ballNear=false;
    }


    

}
